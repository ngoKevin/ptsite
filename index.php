<!DOCTYPE html>
<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
	<meta charset="UTF-8">
	
	<!-- Remove this line if you use the .htaccess -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<meta name="viewport" content="width=device-width">
	
	<meta name="description" content="Designa Studio, a HTML5 / CSS3 template.">
	<meta name="author" content="Sylvain Lafitte, Web Designer, sylvainlafitte.com">
	
	<title>Dr. Stumphrey: PT Extrordinare</title>
	
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	<link rel="shortcut icon" type="image/png" href="favicon.png">
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,700' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="css/style.css">
</head>

<body>

<div class="container">

	<header id="navtop">
	
		<nav class="fright">
			<ul>
				<li><a href="index.php" class="navactive">Home</a></li>
				<li><a href="index.php" class="navactive">About</a></li>
			</ul>
			<ul>
				<li><a href="index.php" class="navactive">Mission</a></li>
				<li><a href="index.php" class="navactive">Location</a></li>
			</ul>
			<ul>
				<li><a href="#widget" class="navactive">Schedule</a></li>
				<li><a href="index.php" class="navactive">Contact</a></li>
			</ul>
		</nav>
	</header>


<div class="home-page main">
	<section class="grid-wrap" >
		<header class="grid col-full">
			<hr>
			<p class="fleft">Home</p>
		</header>

		<div id="widget" class="modalDialog" >
			<div>
				<a href="#close" title="Close">X</a>
				<div id="widget-container" style="text-align: center;">
					<iframe width="1000" height="575" src="http://chronos-corgi.herokuapp.com/#/schedule/widget/52a916dee935a20200000001"></iframe>
				</div>
			</div>
		</div>
		
		<div class="grid col-one-half mq2-col-full">
			<img src="img/drStumpfrey.png" alt="theDoctor" style="max-width: 400px">
			<h1>Dr. Stumphrey</h1>
			<h2>Your Neighborhood Personal Trainer</h2>
			<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Morbi commodo, ipsum sed pharetra gravida, orci magna rhoncus neque, id pulvinar odio lorem non turpis. Nullam sit amet enim. Suspendisse id velit vitae ligula volutpat condimentum. Aliquam erat volutpat. Sed quis velit.
			</p>
			<p>Vivamus pharetra posuere sapien. Nam consectetuer. Sed aliquam, nunc eget euismod ullamcorper, lectus nunc ullamcorper orci, fermentum bibendum enim nibh eget ipsum.
			</p>
		</div>
			
	
		 <div class="slider grid col-one-half mq2-col-full">
		   <div class="flexslider">
		     <div class="slides">
		       <div class="slide">
		           	<figure>
		                 <img src="img/img2.jpg" alt="" style="max-height: 300px">
		                 <figcaption>
		                 	<div>
		                 	<h5>Caption 1</h5>
		                 	<p>Lorem ipsum dolor set amet, lorem, <a href="#">link text</a></p>
		                 	</div>
		                 </figcaption>
		             	</figure>
		           </div>
		           
		           <div class="slide">
		               	<figure>
		                     <img src="img/img.jpg" alt="" style="max-height: 300px">
		                     <figcaption>
		                     	<div>
		                     	<h5>Caption 2</h5>
		                     	<p>Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh.</p>
		                     	</div>
		                     </figcaption>
		                 	</figure>
		               </div>
		            </div>
		   </div>
		 </div>
		
		 </section>

<div class="divide-top">
	<footer class="grid-wrap">
		<ul class="grid col-one-third social">
			<li><a href="#">RSS</a></li>
			<li><a href="#">Facebook</a></li>
			<li><a href="#">Twitter</a></li>
			<li><a href="#">Google+</a></li>
			<li><a href="#">Flickr</a></li>
		</ul>
	
		<div class="up grid col-one-third ">
			<a href="#navtop" title="Go back up">&uarr;</a>
		</div>
		
		<nav class="grid col-one-third ">
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="index.php">About</a></li>
				<li><a href="index.php">Mission</a></li>
				<li><a href="index.php">Location</a></li>
				<li><a href="index.php">Schedule</a></li>
				<li><a href="index.php">Contact</a></li>
			</ul>
		</nav>
	</footer>
</div>

</div>

<!-- Javascript - jQuery -->
<script src="http://code.jquery.com/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="js/jquery-1.7.2.min.js"><\/script>')</script>

<script src="js/jquery.flexslider-min.js"></script>
<script src="js/scripts.js"></script>

<script src="js/widget.js"></script>
<div id="scheduler-widget-container"></div>

</body>
</html>